const {
    check,
    validationResult
} = require('express-validator/check');

const config = require('config');
const Transaction = require('./../models/Transaction');
const Portfolio = require('./../models/Portfolio');

exports.holding = async (req,res)=>{
    let holdings = await Portfolio.find().select('-_id -transactions -__v');
    res.send(holdings);
}


exports.portfolio = async (req,res)=>{
    let portflio = await Portfolio.find().select('-_id -__v -avg_price -quantity').populate('transactions', '-_id -__v -ticker');
    res.send(portflio);
}

exports.returns = async (req, res)=>{
    const current_price = config.get("current_price");
    let returns = await Portfolio.aggregate([
        {
            $match:{}
        },
        {
            $group : {
            _id: "",
            total: {$sum:{ $multiply: [{$subtract : [current_price, "$avg_price"]},"$quantity"]}}
        }},{
            $project: {
                _id: 0,
                total: "$total"
            }
        }
    
            
        
    ]);
    res.send(returns);
}