const {
    validationResult
} = require('express-validator/check');

const Transaction = require('./../models/Transaction');
const Portfolio = require('./../models/Portfolio');

//Add a single transaction
exports.add = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }
    const {
        quantity,
        price,
        t_type
    } = req.body;
    const ticker = req.body.ticker.toUpperCase();

    if (t_type.toUpperCase() !== "BUY" && t_type.toUpperCase() !== "SELL") {
        return res.status(400).json({
            errors: [{
                msg: "Transaction type has to be BUY or SELL"
            }]
        });
    }

    //Transaction type SELL
    if (t_type.toUpperCase() === "SELL") {
        let tickerPresent = await Portfolio.findOne({
            ticker
        });
        if (!tickerPresent) {
            return res.status(400).json({
                errors: [{
                    msg: "Ticker not in holding, can't sell."
                }]
            });
        } else if (tickerPresent.quantity < quantity) {
            return res.status(400).json({
                errors: [{
                    msg: "Can't sell in more quantity than in holding!"
                }]
            });
        } else {
            let transaction = new Transaction({
                ticker,
                quantity,
                t_type,
                price
            });
            await transaction.save();
            tickerPresent.quantity -= quantity;
            if (tickerPresent.quantity === 0)
                tickerPresent.avg_price = 0;
            tickerPresent.transactions.push(transaction);
            await tickerPresent.save();
            return res.send(transaction);
        }

    } else {

        //Transaction type BUY
        let transaction = new Transaction({
            ticker,
            quantity,
            t_type,
            price
        });

        await transaction.save();

        let tickerPresent = await Portfolio.findOne({
            ticker: ticker
        });

        //New Ticker
        if (!tickerPresent) {
            let portfolio = new Portfolio({
                ticker,
                quantity,
                avg_price: price
            });
            portfolio.transactions.push(transaction);
            await portfolio.save();
        } else {
            //Existing Ticker
            let {
                avg_price
            } = tickerPresent;
            let portflio_quantity = tickerPresent.quantity;
            tickerPresent.quantity = portflio_quantity + quantity;
            tickerPresent.avg_price = ((avg_price * portflio_quantity) + (price * quantity)) / tickerPresent.quantity;
            tickerPresent.transactions.push(transaction);
            await tickerPresent.save();

        }
        return res.send(transaction);
    }
}


//Update an existing transaction
exports.update = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }
    const {
        t_id,
        price
    } = req.body;

    let transaction = await Transaction.findOne({
        t_id: t_id
    });
    if (transaction) {
        let holding = await Portfolio.findOne({
            ticker: transaction.ticker
        }).populate('transactions');
        //Transaction type SELL
        if (transaction.t_type.toUpperCase() === "SELL") {
            let old_quantity = transaction.quantity;
            transaction.quantity = req.body.quantity;
            transaction.price = price;
            if ((holding.quantity + old_quantity) < transaction.quantity) {
                return res.status(400).json({
                    errors: [{
                        msg: "Can't sell in more quantity than in holding!"
                    }]
                })
            };
            await transaction.save();
            holding.quantity = holding.quantity + old_quantity - transaction.quantity;
            if (holding.quantity === 0)
                holding.avg_price = 0;
            holding.save();
            return res.send(transaction);
        } else {
            //Transaction type BUY
            let old_price = transaction.price;
            let old_quantity = transaction.quantity;
            transaction.quantity = req.body.quantity;
            transaction.price = price;
            await transaction.save();
            let {
                avg_price,
                quantity
            } = holding;
            let total = avg_price * quantity;
            let revised = total - (old_price * old_quantity);
            holding.quantity = (quantity - old_quantity + transaction.quantity);
            holding.avg_price = (revised + transaction.price * transaction.quantity) / (holding.quantity);
            await holding.save();
            return res.send(transaction);
        }
    } else {
        return res.status(400).json({
            errors: [{
                msg: "Transaction not found."
            }]
        });
    }
}


//Delete an existing transaction
exports.delete = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }
    const {
        t_id,
    } = req.body;

    let transaction = await Transaction.findOne({
        t_id: t_id
    });
    if (transaction) {
        let holding = await Portfolio.findOne({
            ticker: transaction.ticker
        }).select('-transactions');
        let old_price = transaction.price;
        let old_quantity = transaction.quantity;
        await transaction.delete();
        if (transaction.t_type.toUpperCase() === "SELL") {
            holding.quantity = holding.quantity + transaction.quantity;
            if (holding.quantity === 0)
                holding.avg_price = 0;
            await holding.save();
            return res.send(holding);
        } else {
            let {
                avg_price,
                quantity
            } = holding;
            let total = avg_price * quantity;
            let revised = total - (old_price * old_quantity);
            holding.quantity = (quantity - old_quantity);
            if (holding.quantity)
                holding.avg_price = revised / holding.quantity;
            else
                holding.avg_price = 0;
            await holding.save();
            return res.send(holding);
        }
    } else {
        return res.status(400).json({
            errors: [{
                msg: "Transaction not found."
            }]
        });
    }
}