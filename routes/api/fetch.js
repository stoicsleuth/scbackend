const express = require("express");
const router = express.Router();
const fetchController = require('../../controllers/fetchController');



//@route TYPE:GET 
//@address api/fetch/portfolio 
//@access public
//@def To get securities with transactions
router.get("/portfolio",
    fetchController.portfolio
);


//@route TYPE:GET 
//@address api/fetch/holding 
//@access public
//@def To get aggregate value securities
router.get("/holding",
    fetchController.holding
);


//@route TYPE:GET 
//@address api/fetch/returns 
//@access public
//@def To get cumulative returns
router.get("/returns",
    fetchController.returns
);


module.exports = router;