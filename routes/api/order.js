const express = require("express");
const router = express.Router();
const {
    check,
    validationResult
} = require('express-validator/check');
const {body} = require('express-validator');
const transactionController = require('../../controllers/transactionController');
const {
    catchErrors
} = require('./../../handlers/errorHandler');


//@route TYPE:POST 
//@address api/order/add 
//@access public
//@def Adding a transaction of type BUY/SELL
router.post("/add", [
        check('ticker', 'Security ticker is required').not().isEmpty().trim(),
        check('t_type', 'Transaction type is required').not().isEmpty().trim(),
        check('quantity', 'Quantity of stock should not be negative/zero/fractional').isInt({
            gt: 0
        }).trim(),
        check('price', 'Please enter a valid price').isInt({
            gt: 0
        }).trim(),
        check('ticker').trim().custom(value=>{
            if(value.indexOf(' ') >= 0)
                throw new Error('Ticker can only be one word!');
            else
                return true;
        })

    ],
    catchErrors(transactionController.add)
)


//@route TYPE:POST 
//@address api/order/update 
//@access public
//@get Update a transaction with given t_id
router.post("/update", [
        check('t_id', 'Transaction Id is required').not().isEmpty().trim(),
        check('quantity', 'Quantity of stock should not be negative/zero/fractional').isInt({
            gt: 0
        }).trim(),
        check('price', 'Please enter a valid price').isInt({
            gt: 0
        }).trim()
    ],
    catchErrors(transactionController.update)
)


//@route TYPE:POST 
//@address api/order/delete 
//@access public
//@get Delete a transaction with given t_id
router.post("/delete", [
        check('t_id', 'Transaction Id is required').not().isEmpty().trim()
    ],
    catchErrors(transactionController.delete)
)


module.exports = router;