const express = require('express');
const connecDB = require('./config/db');
const errorHandlers = require('./handlers/errorHandler');
const app = express();
var cors = require('cors')


//Connecting to MongoDB
connecDB();


//Initialize Express Bodyparser
app.use(express.json({
    extended: false
}));

//Enable CORS
app.use(cors({credentials: true, origin: true}));

//Define Routes

app.use("/api/order", require("./routes/api/order"));
app.use("/api/fetch", require("./routes/api/fetch"));


// If that above routes don't work, we 404 them and forward to error handler
app.use(errorHandlers.notFound);


// Error handler
app.use(errorHandlers.Errors);



const PORT = process.env.PORT || 8000;

//Start the app
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT} 🍫`);
})