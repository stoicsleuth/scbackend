
//For Async/Await, middleware to pass errors 
exports.catchErrors = (fn) => {
    return function(req, res, next) {
      return fn(req, res, next).catch(next);
    };
}


// Not Found Error Handler
//For any route that is not found, it's marked as 404 and passed along to the next error handler to display
exports.notFound = (req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
};

//Display/send the error
exports.Errors = (err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
      message: err.message
    });
  };;