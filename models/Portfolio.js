const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const portfolioSchema = Schema({
    ticker: {
        type: String,
        required : true
    },
    avg_price: {
        type: Number,
        default: 0
    },
    quantity: {
        type: Number,
        required: true
    },
    transactions : [
        {
            type: mongoose.Schema.ObjectId,
            ref: 'transaction'
        }
    ]
});


module.exports = Portfolio = mongoose.model('portfolio', portfolioSchema);