const mongoose = require("mongoose");
const Schema = mongoose.Schema;


//Defining CounterSchema to keep track of transaction nos
const CounterSchema = Schema({
    c_type: {
        type: String,
        default: 't_counter',
        index: true
    },
    seq: {
        type: Number,
        default: 10000
    }
});

const counter = mongoose.model('counter', CounterSchema);


const transactionSchema = Schema({
    t_id: {
        type: String
    },
    ticker: {
        type: String,
        required: true
    },
    t_type: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})


//Generaing t_id before saving a transaction
transactionSchema.pre('save', async function (next) {
    var present_transaction = this;
    try {
        if (!present_transaction.t_id) {
            let count = await counter.findOneAndUpdate({
                c_type: 't_counter'
            }, {
                $inc: {
                    seq: 1
                }
            }, {
                new: true
            });
            present_transaction.t_id = present_transaction.t_type.toUpperCase()==="BUY" ? `TB${count.seq}` : `TS${count.seq}` ;
            next();
        }
    } catch (err) {
        return next(err);
    }
});



module.exports = Transaction = mongoose.model('transaction', transactionSchema);